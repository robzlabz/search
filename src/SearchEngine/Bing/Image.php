<?php

namespace RAFamily\SearchEngine\Bing;

use RAFamily\SearchEngine\Network;
use RAFamily\SearchEngine\SearchImageContract;
use Symfony\Component\DomCrawler\Crawler;

class Image implements SearchImageContract
{
    public function get($keyword)
    {
        $keyword = urlencode($keyword);
        $url = 'https://www.bing.com/images/async?q='.$keyword.'&first=0&count=100';
        $response = (new Network)->get($url);

        return $this->render($response);
    }

    public function render($response)
    {
        $json = [];
        $strings = [];
        $crawler = new Crawler($response, null, 'https://www.bing.com');

        $crawler->filter('.iusc')->each(function (Crawler $node) use (&$json,&$strings) {
            $json[] = $node->attr('m');
            $strings[] = $node->attr('href');
        });
        $json = '['.implode(',', $json).']';

        return $this->prettify($json, $strings);
    }

    public function prettify($json, $strings)
    {
        $dimensions = [];
        foreach ($strings as $key => $link) {
            foreach (explode('&', $link) as $query) {
                if (strpos($query, 'exph=') !== false) {
                    $dimensions[$key]['height'] = str_replace('exph=', '', $query);
                }
                if (strpos($query, 'expw=') !== false) {
                    $dimensions[$key]['width'] = str_replace('expw=', '', $query);
                }
            }
        }

        $items = [];
        foreach (json_decode($json) as $key => $item) {
            $filetype = explode('.', $item->murl);

            $items[] = [
                'host' => parse_url($item->murl, PHP_URL_HOST),
                'image' => $item->murl,
                'filetype' => end($filetype),
                'url' => $item->purl,
                'width' => $dimensions[$key]['width'],
                'height' => $dimensions[$key]['height'],
                'title' => $item->t
            ];
        }

        return $items;
    }


}
