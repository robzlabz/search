<?php


namespace RAFamily\SearchEngine\Google;


use Exception;
use RAFamily\SearchEngine\Network;
use RAFamily\SearchEngine\SearchEverythingContract;
use Symfony\Component\DomCrawler\Crawler;

class Everything implements SearchEverythingContract
{

    public function get($keyword)
    {
        $keyword = urlencode($keyword);
        $url = 'https://www.google.com/search?q='.$keyword;
        try {
            $response = (new Network())->get($url);
            return $this->render($response);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function render($response)
    {
        $crawler = new Crawler($response,null,'https://www.google.com');

        $results = [];
        $crawler->filter('.g')->each(function (Crawler $node) use(&$results){
            try {
                $results[] = [
                    'title' => $node->filter('h3')->text(),
                    'url' => $node->filter('.r a')->first()->attr('href'),
                    'description' => $node->filter('span.st')->text()
                ];
            } catch (Exception $e) {}

        });

        return $this->prettify($results);
    }

    public function prettify($response)
    {
        return [
            'data' => $response,
            'meta' => [
                'indexed' => count($response) > 0,
                'count' => count($response)
            ]
        ];
    }
}