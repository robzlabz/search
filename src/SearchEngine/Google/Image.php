<?php

namespace RAFamily\SearchEngine\Google;

use RAFamily\SearchEngine\Network;
use RAFamily\SearchEngine\SearchImageContract;
use Symfony\Component\DomCrawler\Crawler;

class Image implements SearchImageContract
{
    public function get($keyword)
    {
        $keyword = urlencode($keyword);
        $url = 'https://www.google.com/search?q='.$keyword.'&tbm=isch';
        $response = (new Network)->get($url);

        return $this->render($response);
        // return new SearchImage($this->render($response));
    }

    public function render($response)
    {
        $json = [];
        $crawler = new Crawler($response, null, 'https://www.google.com');
        $crawler->filter('.rg_meta')->each(function (Crawler $node) use (&$json) {
            $json[] = $node->text();
        });
        $json = '['.implode(',', $json).']';

        return $this->prettify($json);
    }

    public function prettify($response)
    {
        $items = [];
        foreach (json_decode($response) as $item) {
            $items[] = [
                'host' => $item->isu,
                'image' => $item->ou,
                'filetype' => $item->ity,
                'url' => $item->ru,
                'width' => $item->ow,
                'height' => $item->oh,
                'title' => $item->pt
            ];
        }

        return $items;
    }


}
