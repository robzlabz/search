<?php

namespace RAFamily\SearchEngine;

interface SearchImageContract
{
    public function get($keyword);
    public function render($response);
    public function prettify($response);
}
