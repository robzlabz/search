<?php

namespace RAFamily\SearchEngine\Yahoo;

use RAFamily\SearchEngine\Network;
use RAFamily\SearchEngine\SearchImageContract;
use Symfony\Component\DomCrawler\Crawler;

class Image implements SearchImageContract
{
    public function get($keyword)
    {
        $keyword = urlencode($keyword);
        $url = 'https://images.search.yahoo.com/search/images?ei=UTF-8&fr=sfp&fr2=sb-top-images.search&o=js&p='.$keyword.'&tmpl=&nost=1&b=0&iid=Y.1&';
        $response = (new Network)->get($url);

        return $this->render($response);
    }

    private function render($response)
    {
        $html_json = json_decode($response);

        $data = [];
        $crawler = new Crawler($html_json->html, null, 'https://images.search.yahoo.com');
        $crawler->filter('.ld')->each(function (Crawler $node) use (&$data) {
            $data[] = $node->attr('data');
        });

        $data = '['.implode(',', $data).']';

        return $this->prettify($data);
    }

    public function prettify($response)
    {
        $items = [];
        foreach (json_decode($response) as $item) {
            $filetype = explode('.', $item->iurl);

            $items[] = [
                'host' => $item->a,
                'image' => $item->iurl,
                'filetype' => end($filetype),
                'url' => $item->rurl,
                'width' => $item->w,
                'height' => $item->h,
                'title' => $item->alt
            ];
        }

        return $items;
    }
}
